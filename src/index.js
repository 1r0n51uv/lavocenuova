import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import firebase from 'firebase/app';
import registerServiceWorker from './registerServiceWorker';
import { FirestoreProvider } from 'react-firestore';

const root = document.getElementById('root');


const config = {
    apiKey: "AIzaSyDp1VlT7jt0zSmKKiB005wfuXoRWy6-j60",
    authDomain: "thevoice-aea7e.firebaseapp.com",
    databaseURL: "https://thevoice-aea7e.firebaseio.com",
    projectId: "thevoice-aea7e",
    storageBucket: "thevoice-aea7e.appspot.com",
    messagingSenderId: "1016122504286"
};


firebase.initializeApp(config);

ReactDOM.render(<FirestoreProvider firebase={firebase}> <App /> </FirestoreProvider>, root);
registerServiceWorker();


